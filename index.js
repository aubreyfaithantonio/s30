// Using Count Operator - fruitsOnSale
db.fruits.aggregate([
	{
		$match: {onSale: true}
	},
	{
		$count: "fruitsOnSale"
	}
])

// Using Count Operator - enoughStock
db.fruits.aggregate([
	{
		$match: { 
			stock: {
				$gte: 20
			}
		}
	},
	{
		$count: "enoughStock"
	}
])

// Using Average Operator
db.fruits.aggregate([
	{
		$match: {onSale: true}	
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_price: {
				$avg: "$price"
			}
		}
	}
])

// Using Max Operator
db.fruits.aggregate([
	{
		$group: {
			_id: "$supplier_id",
			max_price: {
				$max: "$price"
			}
		}
	}
])
// Using Min Operator
db.fruits.aggregate([
	{
		$group: {
			_id: "$supplier_id",
			min_price: {
				$min: "$price"
			}
		}
	}
])